
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <fstream>
#include <caffe/caffe.hpp>
#include "caffe_classify.hpp"


using namespace std;
using namespace cv;


Classifier::Classifier(const string& model_file,
                       const string& trained_file,
                       const string& mean_file,
                       const string& label_file) {
#ifdef CPU_ONLY
  Caffe::set_mode(Caffe::CPU);
#else
  Caffe::set_mode(Caffe::GPU);
#endif

  /* Load the network. */
  net_.reset(new Net<float>(model_file, TEST));
  net_->CopyTrainedLayersFrom(trained_file);

  CHECK_EQ(net_->num_inputs(), 1) << "Network should have exactly one input.";
  CHECK_EQ(net_->num_outputs(), 1) << "Network should have exactly one output.";

  Blob<float>* input_layer = net_->input_blobs()[0];
  num_channels_ = input_layer->channels();
  cout << num_channels_ << endl;
  CHECK(num_channels_ == 3 || num_channels_ == 1)
    << "Input layer should have 1 or 3 channels.";
  input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
  cout << input_geometry_ << endl;
  /* Load the binaryproto mean file. */
  SetMean(mean_file);

  /* Load labels. */
  std::ifstream labels(label_file.c_str());
  CHECK(labels) << "Unable to open labels file " << label_file;
  string line;
  while (std::getline(labels, line))
    labels_.push_back(string(line));

  Blob<float>* output_layer = net_->output_blobs()[0];
  CHECK_EQ(labels_.size(), output_layer->channels())
    << "Number of labels is different from the output layer dimension.";
}

Size Classifier::mean_size()
{
	return mean_.size();
}

static bool PairCompare(const std::pair<float, int>& lhs,
                        const std::pair<float, int>& rhs) {
  return lhs.first > rhs.first;
}

/* Return the indices of the top N values of vector v. */
static std::vector<int> Argmax(const std::vector<float>& v, int N) {
  std::vector<std::pair<float, int> > pairs;
  for (size_t i = 0; i < v.size(); ++i)
    pairs.push_back(std::make_pair(v[i], i));
  std::partial_sort(pairs.begin(), pairs.begin() + N, pairs.end(), PairCompare);

  std::vector<int> result;
  for (int i = 0; i < N; ++i)
    result.push_back(pairs[i].second);
  return result;
}

/* Return the top N predictions. */
std::vector<Prediction> Classifier::Classify(const cv::Mat& img, int N) {
  std::vector<float> output = Predict(img);
	
  std::vector<int> maxN = Argmax(output, N);
  std::vector<Prediction> predictions;

  for (int i = 0; i < N; ++i) {
    int idx = maxN[i];
    predictions.push_back(std::make_pair(labels_[idx], output[idx]));
  }

  return predictions;
}

void Classifier::WrapInputLayer(std::vector<vector<cv::Mat> > &input_channels, int image_num) {
	Blob<float>* input_layer = net_->input_blobs()[0];

	int width = input_layer->width();
	int height = input_layer->height();
	float* input_data = input_layer->mutable_cpu_data();
	for (int img = 0; img < image_num; img++) {
		for (int i = 0; i < input_layer->channels(); ++i) {
			cv::Mat channel(height, width, CV_32FC1, input_data);
			input_channels[img].push_back(channel);
			input_data += width * height;
		}
	}
}

/*Output prediction for each input image*/
vector<Prediction> Classifier::Classify(vector<Mat> images)
{
	Blob<float>* input_layer = net_->input_blobs()[0];
	input_layer->Reshape(images.size(), num_channels_,
		input_geometry_.height, input_geometry_.width);
	
	net_->Reshape();
	
	vector<vector<Mat> > input_channels;
	for (int i = 0; i< images.size(); i++) {
		vector<Mat> new_vec;
		input_channels.push_back(new_vec);
	}
	
	WrapInputLayer(input_channels, images.size());

	Preprocess(images, input_channels);

	net_->Reshape();

	net_->ForwardPrefilled();
		
	Blob<float>* output_layer = net_->output_blobs()[0];
	
	const float* begin = output_layer->cpu_data();
	
	vector<Prediction> predictions;

 
	for (int example = 0; example < images.size(); example++) {
		const float *label_begin = begin + (example * output_layer->channels());
		int max_index = -1;
		float max_val = 0;
		for (int i = 0; i < output_layer->channels(); i++) {
			if (label_begin[i] > max_val) {
				max_val = label_begin[i];
				max_index = i;
			}
		}
		predictions.push_back(std::make_pair(labels_[max_index], max_val));
		
	}
	return predictions;		
}

/* Load the mean file in binaryproto format. */
void Classifier::SetMean(const string& mean_file) {
  BlobProto blob_proto;
  ReadProtoFromBinaryFileOrDie(mean_file.c_str(), &blob_proto);

  /* Convert from BlobProto to Blob<float> */
  Blob<float> mean_blob;
  mean_blob.FromProto(blob_proto);
  cout << mean_blob.channels() << endl;
  CHECK_EQ(mean_blob.channels(), num_channels_)
    << "Number of channels of mean file doesn't match input layer.";

  /* The format of the mean file is planar 32-bit float BGR or grayscale. */
  std::vector<cv::Mat> channels;
  float* data = mean_blob.mutable_cpu_data();
  for (int i = 0; i < num_channels_; ++i) {
    /* Extract an individual channel. */
    cv::Mat channel(mean_blob.height(), mean_blob.width(), CV_32FC1, data);
    channels.push_back(channel);
    data += mean_blob.height() * mean_blob.width();
  }

  /* Merge the separate channels into a single image. */
  cv::Mat mean;
  cv::merge(channels, mean);

  /* Compute the global mean pixel value and create a mean image
   * filled with this value. */
  cv::Scalar channel_mean = cv::mean(mean);
  mean_ = cv::Mat(input_geometry_, mean.type(), channel_mean);
}

std::vector<float> Classifier::Predict(const cv::Mat& img) {
  Blob<float>* input_layer = net_->input_blobs()[0];
  input_layer->Reshape(1, num_channels_,
                       input_geometry_.height, input_geometry_.width);
  /* Forward dimension change to all layers. */
  net_->Reshape();

  std::vector<cv::Mat> input_channels;
  WrapInputLayer(&input_channels);

  Preprocess(img, &input_channels);

  net_->ForwardPrefilled();

  /* Copy the output layer to a std::vector */
  Blob<float>* output_layer = net_->output_blobs()[0];
  const float* begin = output_layer->cpu_data();
  const float* end = begin + output_layer->channels();
  return std::vector<float>(begin, end);
}

/* Wrap the input layer of the network in separate cv::Mat objects
 * (one per channel). This way we save one memcpy operation and we
 * don't need to rely on cudaMemcpy2D. The last preprocessing
 * operation will write the separate channels directly to the input
 * layer. */
void Classifier::WrapInputLayer(std::vector<cv::Mat>* input_channels) {
  Blob<float>* input_layer = net_->input_blobs()[0];

  int width = input_layer->width();
  int height = input_layer->height();
  float* input_data = input_layer->mutable_cpu_data();
  for (int i = 0; i < input_layer->channels(); ++i) {
    cv::Mat channel(height, width, CV_32FC1, input_data);
    input_channels->push_back(channel);
    input_data += width * height;
  }
}

void Classifier::Preprocess(vector<Mat> images,
                            vector<vector<cv::Mat> > &input_channels) {
/* Convert the input image to the input image format of the network. */
	for (int image_num = 0; image_num < images.size(); image_num++) {
		
		Mat img = images[image_num];	
		cv::Mat sample;
		if (img.channels() == 3 && num_channels_ == 1)
			cv::cvtColor(img, sample, CV_BGR2GRAY);
		else if (img.channels() == 4 && num_channels_ == 1)
			cv::cvtColor(img, sample, CV_BGRA2GRAY);
		else if (img.channels() == 4 && num_channels_ == 3)
			cv::cvtColor(img, sample, CV_BGRA2BGR);
		else if (img.channels() == 1 && num_channels_ == 3)
			cv::cvtColor(img, sample, CV_GRAY2BGR);
		else
			sample = img;

		cv::Mat sample_resized;

		if (sample.size() != input_geometry_)
			cv::resize(sample, sample_resized, input_geometry_);
		else
			sample_resized = sample;

		cv::Mat sample_float;
		if (num_channels_ == 3)
			sample_resized.convertTo(sample_float, CV_32FC3);
		else
			sample_resized.convertTo(sample_float, CV_32FC1);

		cv::Mat sample_normalized;
		if (!mean_.empty())
			cv::subtract(sample_float, mean_, sample_normalized);
		else
			sample_normalized = sample_float;
		/* This operation will write the separate BGR planes directly to the
		* input layer of the network because it is wrapped by the cv::Mat
		* objects in input_channels. */
		cv::split(sample_normalized, input_channels[image_num]);

		/*CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
			== net_->input_blobs()[0]->cpu_data())
		<< "Input channels are not wrapping the input layer of the network.";*/
	}
}

void Classifier::Preprocess(const cv::Mat& img,
                            std::vector<cv::Mat>* input_channels) {
  /* Convert the input image to the input image format of the network. */
  cv::Mat sample;
  if (img.channels() == 3 && num_channels_ == 1)
    cv::cvtColor(img, sample, CV_BGR2GRAY);
  else if (img.channels() == 4 && num_channels_ == 1)
    cv::cvtColor(img, sample, CV_BGRA2GRAY);
  else if (img.channels() == 4 && num_channels_ == 3)
    cv::cvtColor(img, sample, CV_BGRA2BGR);
  else if (img.channels() == 1 && num_channels_ == 3)
    cv::cvtColor(img, sample, CV_GRAY2BGR);
  else
    sample = img;

  cv::Mat sample_resized;
  if (sample.size() != input_geometry_) {
  	cout << "Trying to resize!" << endl;
  	cout << "input geometry: " << input_geometry_ << endl;
  	cout << "sample size: " <<  sample.size() << endl;
    cv::resize(sample, sample_resized, input_geometry_);
 }
  else
    sample_resized = sample;

  cv::Mat sample_float;
  if (num_channels_ == 3)
    sample_resized.convertTo(sample_float, CV_32FC3);
  else
    sample_resized.convertTo(sample_float, CV_32FC1);

  cv::Mat sample_normalized;
  cv::subtract(sample_float, mean_, sample_normalized);

  /* This operation will write the separate BGR planes directly to the
   * input layer of the network because it is wrapped by the cv::Mat
   * objects in input_channels. */
  cv::split(sample_normalized, *input_channels);

  CHECK(reinterpret_cast<float*>(input_channels->at(0).data)
        == net_->input_blobs()[0]->cpu_data())
    << "Input channels are not wrapping the input layer of the network.";
    
	cout << "Done with preprocess!" << endl;
}


void read_images_labels(string input_file_name, vector<Mat> &images, vector<string> &labels,
			vector<string> &image_names, string root_name, int resize_h, int resize_w, int gray)
{
	string temp_name = input_file_name;
	ifstream input(temp_name.c_str());
	int height = 77;
	int width = 62;
	
	if (resize_h != -1)
		height = resize_h;
	
	if (resize_w != -1)
		width = resize_w;
	
	while (!input.eof()) {	
		string temp_name;
		string temp_label;
		input >> temp_name >> temp_label;
		cout << root_name + temp_name << endl;
		string cur_name = root_name + temp_name;
		Mat temp;
		temp = imread(cur_name);
		if (!temp.empty()) {
			if (gray)
				cvtColor(temp, temp, CV_BGR2GRAY);
			Mat dst;
			resize(temp, dst, Size(width, height));
			
			images.push_back(dst);
			image_names.push_back(root_name + temp_name);
			labels.push_back(temp_label);
		}
	}
}

void classify_batch_images(Classifier *classifier, vector<Mat> &images, vector<string> &labels,
	vector<string> &image_names, string error_file, int batch_size)
{
	ofstream output(error_file.c_str(), ofstream::app);
	double total_checked = (double) images.size();
	double total_errors = 0;
	//for (int i = 0; i < images.size(); i++) {
	
	int output_size = classifier->labels_.size();
	int confusion_array[output_size][output_size];
	int vote_arr[output_size][output_size];
	for (int i = 0; i < output_size; i++) {
		for (int j = 0; j < output_size; j++) {
			confusion_array[i][j] = 0;
			vote_arr[i][j] = 0;
		}
	}
	
	int certainty_arr[4][2];
	
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++)
			certainty_arr[i][j] = 0;
	}
	
	
	vector<int> true_label;
	vector<int> votes;
	
	int current_index = -1;
	int begin_cycle = 3;
	
		
	int cur_index = 0; 
	while (1) {
		vector<Mat> test_images;
		for (int i = cur_index; i < cur_index + batch_size && i < images.size(); i++) {
			test_images.push_back(images[i]);
		}

		
		vector<Prediction> predictions = classifier->Classify(test_images);
		
		for (int i = cur_index; i < cur_index + batch_size && i < images.size(); i++) {
			Prediction p = predictions[i];
		
			int row = atoi(labels[i].c_str());
			int col = atoi(p.first.c_str());

			confusion_array[row][col] += 1;
			
			if (labels[i].compare(p.first) != 0) {
				total_errors += 1;
				cout << image_names[i] << " |" << p.second << "| " << labels[i] << " " << p.first << endl;
				output <<  image_names[i] << " " << p.second << " " << labels[i] << " " << endl;
			}
		
			
		}
	}

	cout << "Accuracy: " << total_errors / total_checked << endl;

	cout << "Row: GT label, Col: Detected Label" << endl;

	for (int i = 0; i < output_size; i++) {
		for (int j = 0; j < output_size; j++) {
			cout << "[ " << confusion_array[i][j] << " ]";
		}
		cout << endl;
	}
}

void classify_images(Classifier *classifier, vector<Mat> &images, vector<string> &labels, vector<string> &image_names, string error_file)
{
	ofstream output(error_file.c_str(), ofstream::app);
	double total_checked = (double) images.size();
	double total_errors = 0;
	//for (int i = 0; i < images.size(); i++) {
	
	int output_size = classifier->labels_.size();
	int confusion_array[output_size][output_size];
	int vote_arr[output_size][output_size];
	for (int i = 0; i < output_size; i++) {
		for (int j = 0; j < output_size; j++) {
			confusion_array[i][j] = 0;
			vote_arr[i][j] = 0;
		}
	}
	
	int certainty_arr[4][2];
	
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++)
			certainty_arr[i][j] = 0;
	}
	
	
	vector<int> true_label;
	vector<int> votes;
	
	int current_index = -1;
	int begin_cycle = 3;
	
		
	for (int i = 0; i < images.size(); i++) {
		/*if (begin_cycle == 0 || begin_cycle == 3) {
			true_label.push_back(atoi(labels[i].c_str()));
			votes.push_back(0);
			current_index += 1;
			begin_cycle = 0;
		}*/
		
		cout << images[i].size() << endl;

		vector<Prediction> predictions = classifier->Classify(images[i], 2);
		

		Prediction p = predictions[0];
		
		int row = atoi(labels[i].c_str());
		int col = atoi(p.first.c_str());
		
		//votes[current_index] += col;
		
		confusion_array[row][col] += 1;
		
		if (labels[i].compare(p.first) != 0) {
			total_errors += 1;
			cout << image_names[i] << " |" << p.second << "| " << labels[i] << " " << p.first << endl;
			output <<  image_names[i] << " " << p.second << " " << labels[i] << " " << endl;
		}
		
	}
	
	cout << "Accuracy: " << total_errors / total_checked << endl;
	
	cout << "Row: GT label, Col: Detected Label" << endl;
	
	for (int i = 0; i < output_size; i++) {
		for (int j = 0; j < output_size; j++) {
			cout << "[ " << confusion_array[i][j] << " ]";
		}
		
		cout << endl;
	}
	/*
	for (int i = 0; i < votes.size(); i++) {
		int vote_guess = 0;
		
		if (votes[i] > 1)
			vote_guess = 1;
			
		vote_arr[true_label[i]][vote_guess] += 1;
		
		certainty_arr[votes[i]][true_label[i]] += 1;
	}
	
	cout << "Row: GT label, Col: Vote Label" << endl;
	
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			cout << "[ " << vote_arr[i][j] << " ]";
		}
		
		cout << endl;
	}
	
	cout << "Row: Votes for label 1, Col: True Label" << endl;
	
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++) {
			cout << "[ " << certainty_arr[i][j] << " ]";
		}
		
		cout << endl;
	}*/
	
	
}

int main(int argc, char** argv) {
	if (argc < 4) {
		std::cerr << "Usage: " << argv[0];
		std::cerr << " config_file.txt test_image_file root_folder  [OPTIONS]" << endl;
		std::cerr << "Options: \n\t--resize_width=<int>" << endl;
		std::cerr << "\t--resize_height=<int>" << endl;
		std::cerr << "\t--grayscale" <<  endl;
		std::cerr << "\t--error_file" << endl;
			//<< " deploy.prototxt network.caffemodel"
			//<< " mean.binaryproto labels.txt test_image_file root_folder" << std::endl;
		return 1;
	}
	
	int overwrite_flag = 1;

	::google::InitGoogleLogging(argv[0]);

	ifstream config_file(argv[1]);
	string model_file, trained_file, mean_file, label_file;
	
	config_file >> model_file;
	config_file >> trained_file;
	config_file >> mean_file;
	config_file >> label_file;

	
	Classifier classifier(model_file, trained_file, mean_file, label_file);

	string test_image_file = argv[2];
	string root_folder = argv[3];
	
	std::cout << "---------- Prediction for "
		<< test_image_file << " ----------" << std::endl;

	vector<Mat> images;
	vector<string> labels;
	vector<string> file_names;
	int resize_h = -1;
	int resize_w = -1;
	int gray = 0;
	string error_file;
	std::string::size_type sz;
	string temp = "--resize_height=55";
	
	size_t t = temp.find("--resize_height=");
	cout << t << endl;
	for (int i = 4; i < argc; i++) {
		//string arg_str(argv[i]);
		string arg_str = argv[i];
		size_t found = arg_str.find("--resize_height=");

		if (found != string::npos) {
			int bias = strlen("--resize_height=");
			int num_length = arg_str.length() - bias;
			resize_h = atoi(arg_str.substr(bias, num_length).c_str());
			cout << "Resize h " << resize_h << endl;
			continue;
		}
		
		found = arg_str.find("--resize_width=");
		
		if (found != string::npos) {
			int bias = strlen("--resize_width=");
			int num_length = arg_str.length() - bias;
			resize_w = atoi(arg_str.substr(bias, num_length).c_str());
			cout << "Resize w " << resize_w << endl;
			continue;
		}
		
		found = arg_str.find("--grayscale");
		if (found != string::npos) {
			gray = 1;
			continue;
		}

		found = arg_str.find("--error_file=");
		
		if (found != string::npos) {
			int bias = strlen("--error_file=");
			int num_length = arg_str.length() - bias;
			error_file = arg_str.substr(bias, num_length);
			cout << error_file << endl;		
		}
		
	}

	if (resize_h != -1)
		cout << "Resizing height to: " <<  resize_h << endl;
	
	if (resize_w != -1)
		cout << "Resizing width to: " << resize_w << endl;

	if (gray)
		cout << "Converting input images to grayscale" << endl;

		
	read_images_labels(test_image_file, images, labels, file_names, root_folder, resize_h, resize_w, gray);
	
	cout << "Classifying images!" << endl;
	classify_batch_images(&classifier, images, labels, file_names, error_file, 64);
	
	/*
	cv::Mat img = cv::imread(file, -1);
	CHECK(!img.empty()) << "Unable to decode image " << file;
	std::vector<Prediction> predictions = classifier.Classify(img);

	for (size_t i = 0; i < predictions.size(); ++i) {
		Prediction p = predictions[i];
		std::cout << std::fixed << std::setprecision(4) << p.second << " - \""
		<< p.first << "\"" << std::endl;
	}*/
	
}
