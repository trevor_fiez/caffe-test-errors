using namespace caffe;  // NOLINT(build/namespaces)
using std::string;

/* Pair (label, confidence) representing a prediction. */
typedef std::pair<string, float> Prediction;

class Classifier {
 public:
  Classifier(const string& model_file,
             const string& trained_file,
             const string& mean_file,
             const string& label_file);

  std::vector<Prediction> Classify(const cv::Mat& img, int N = 5);
	cv::Size mean_size();
  std::vector<Prediction> Classify(std::vector<cv::Mat> images);
    std::vector<string> labels_;
 private:
  void SetMean(const string& mean_file);

  std::vector<float> Predict(const cv::Mat& img);

  void WrapInputLayer(std::vector<cv::Mat>* input_channels);

  void Preprocess(const cv::Mat& img,
                  std::vector<cv::Mat>* input_channels);

  void WrapInputLayer(std::vector<vector<cv::Mat> > &input_channels, int image_num);

  
  void Preprocess(std::vector<cv::Mat> images,
                            std::vector<std::vector<cv::Mat> > &input_channels);


   

 private:
  shared_ptr<Net<float> > net_;
  cv::Size input_geometry_;
  int num_channels_;
  cv::Mat mean_;
};
