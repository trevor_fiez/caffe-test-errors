# README #

Adjust the makefile for your system.

Caffe and Opencv are required

### Classifying Images in Caffe ###

* Takes in a list of images and target labels and creates a confusion matrix and outputs incorrectly classified images to a test file
* Performs transformation parameters that you might have used when created your test dataset
* To see options call the program with no arguments
* An example configure file and label file is included in the repo
* Currently, the labels must be ints from 0 to whatever the number of labels are

### References and Code ###

Much of the code included in this repo is from the caffe_classify.cpp, available from Berkley's vision groups page.

### Questions? ###

Please email Trevor Fiez at fieztr@oregonstate.edu